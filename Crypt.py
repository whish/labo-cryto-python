import os
import sys
import zlib
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto import Random


def crc(fileName):
    prev = 0
    for eachLine in open(fileName,"rb"):
        prev = zlib.crc32(eachLine, prev)
    return "%X"%(prev & 0xFFFFFFFF)

# Encryption of files
def encrypt(key, filename):
    chunksize = 64 * 1024
    outputFile = 'file.crypt'
    filesize = str(os.path.getsize(filename)).zfill(16)
    IV = Random.new().read(16)

    encryptor = AES.new(key, AES.MODE_CBC, IV)

    with open(filename, 'rb') as infile:
        with open(outputFile, 'wb') as outfile:
            outfile.write(filesize.encode('utf-8'))
            outfile.write(IV)

            while True:
                chunk = infile.read(chunksize)

                if len(chunk) == 0:
                    break
                elif len(chunk) % 16 != 0:
                    chunk += b' ' * (16 - (len(chunk) % 16))

                outfile.write(encryptor.encrypt(chunk))


# Decryption of files
def decrypt(key, filename):
    chunksize = 64 * 1024
    outputFile = 'file.decrypt'

    with open(filename, 'rb') as infile:
        filesize = int(infile.read(16))
        IV = infile.read(16)

        decryptor = AES.new(key, AES.MODE_CBC, IV)

        with open(outputFile, 'wb') as outfile:
            while True:
                chunk = infile.read(chunksize)

                if len(chunk) == 0:
                    break

                outfile.write(decryptor.decrypt(chunk))
            outfile.truncate(filesize)


def getKey(password):
    hasher = SHA256.new(password.encode('utf-8'))
    return hasher.digest()

def main():
    action = sys.argv[1]
    password = sys.argv[2]
    inFile = sys.argv[3]

    print('action: ' + action + ' password: ' + password)
    print('crc: ' + crc(inFile));

    if action == 'encrypt':
        encrypt(getKey(password), inFile)
    elif action == 'decrypt':
        decrypt(getKey(password), inFile)


if __name__ == '__main__':
    main()